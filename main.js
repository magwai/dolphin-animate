Modernizr.load([
    {
        test: Modernizr.input.placeholder,
        nope: '/js/jquery.placeholder.js',
        complete: function(){
            $(function(){
                if (!Modernizr.input.placeholder){
                    $('[placeholder]').placeholder();
                }
            })
        }
    },
    {
        test: $('html').hasClass('ie8'),
        yep: ['/js/PIE_IE678.js', '/js/selectivizr-min.js', '/js/respond.js'],
        complete: function(){
            $(function(){
                if (window.PIE) {
                    $('.pie').each(function() {
                        PIE.attach(this);
                    });
                }
            })
        }
    }
]);

$(function(){

    //Стилизация селектов
    $.ikSelect.extendDefaults({autoWidth: true, ddFullWidth: false, dynamicWidth: false, extractLink: false});
    $('.style-select').ikSelect({customClass: 'style-select'});
    $('.style-select2').ikSelect({autoWidth: false, customClass: 'style-select2'});

    //Модальные окна
    $.arcticmodal('setDefault', {
        overlay: {
            css: {
                backgroundColor: '#d0d0d0',
                opacity: 0
            }
        },
        afterLoadingOnShow: function(data, el) {
            if (!Modernizr.input.placeholder){
                $('.modal-win [placeholder]').placeholder();
            }
            if (window.PIE) {
                $('.modal-win .pie').each(function() {
                    PIE.attach(this);
                });
            }
            $('.modal-win .style-select').ikSelect();
        }
    });

    $('body').on('click', '.show-modal', function(){
        var url = $(this).attr('href');
        $.arcticmodal({
            type: 'ajax',
            url: url
        });
        return false;
    });

	function sbmt_f(form) {
		$.ajax({
			url: $(form).attr('action'),
			data: $(form).serializeArray(),
			method: 'post',
			dataType: 'html',
			cache: false,
			success: function(html){
                $(form).html($(html).html());
				if(!$(form).find('.error').length) window.location = '/';
            }
		});
		return false;
	};

	var clink;


    $('body').on('click', '.show-form', function(){
        $('.no-modal').remove();
        var link = $(this);
		var url = '/feedback';
        $.ajax({
            url: url,
            cache: false,
            success: function(html){
				var frm;
				clink = link;
				if(link.hasClass('show-on-center')) {
					var hh = $(window).height() / 2 + $(window).scrollTop() - 250;
					frm = $(html).addClass('no-modal').css({'position':'absolute', 'top':hh+'px', 'left':$('.wrap-site > .container').offset().left + 286/*'left':link.offset().left*/, 'margin':'20px 0 0 -35px'}).hide()
						.appendTo('body').fadeIn(300).submit(function() {
							sbmt_f(this);
							return false;
						});
				}
				else if($('.b-about').length) {
					frm = $(html).addClass('no-modal').css({'position':'absolute', 'top':link.offset().top - 460, 'left':$('.wrap-site > .container').offset().left + 176, 'margin':'20px 0 0 -35px'}).hide()
						.appendTo('body').fadeIn(300).submit(function() {
							sbmt_f(this);
							return false;
						});
				}
				else if(!$('body').hasClass('contact-page')) {
					frm = $(html).addClass('no-modal').css({'position':'absolute', 'top':link.offset().top, 'left':$('.wrap-site > .container').offset().left + 286/*'left':link.offset().left*/, 'margin':'20px 0 0 -35px'}).hide()
						.appendTo('body').fadeIn(300).submit(function() {
							sbmt_f(this);
							return false;
						});
				}
				else {
					frm = $(html).addClass('no-modal').css({'position':'absolute', 'top':link.offset().top, 'left':$('.wrap-site > .container').offset().left + 286, 'margin':'20px 0 0 -35px'}).hide()
						.appendTo('body').fadeIn(300).submit(function() {
							sbmt_f(this);
							return false;
						});
				}
				if(link.data('bank') && link.data('credit')) {
					frm.find('textarea[name="message"]').val(link.data('bank') + ' ' + link.data('credit'));
				}
            }
        });
    });

	$(window).resize(function(){
		if($('.no-modal').length) {
			if(clink.hasClass('show-on-center')) {
				$('.no-modal').css({'left':$('.wrap-site > .container').offset().left + 286});
			}
			else if($('.b-about').length) {
				$('.no-modal').css({'left':$('.wrap-site > .container').offset().left + 176});
			}
			else if(!$('body').hasClass('contact-page')) {
				$('.no-modal').css({'left':$('.wrap-site > .container').offset().left + 286});
			}
			else {
				$('.no-modal').css({'left':$('.wrap-site > .container').offset().left + 286});
			}
		}
	});

    $('body').on('click', '.no-modal .close-btn', function(){
       $(this).closest('.no-modal').remove();
    });

	$('body').on('change', 'select.churl', function() {
		window.location = $(this).val();
	});

	$('body').on('change', 'select.churl1', function() {
		if($(this).val() != 'vklad' && $(this).val() != 'ipoteka' && $(this).val() != 'consumercredit' && $(this).val() != 'autocredit'
				&& $(this).val() != 'creditcard')
			window.location = $(this).val();

		if($(this).val() == 'ipoteka') {
			$.ajax({
				url: '/getformipoteka',
				cache: false,
				success: function(html){
					$('form#validate').replaceWith(html);
					if (!Modernizr.input.placeholder){
						$('form#validate [placeholder]').placeholder();
					}
					if (window.PIE) {
						$('form#validate .pie').each(function() {
							PIE.attach(this);
						});
					}
					$('form#validate .style-select').ikSelect({customClass: 'style-select'});
					$('form#validate .style-select2').ikSelect({autoWidth: false, customClass: 'style-select2'});
				}
			});
		}

		if($(this).val() == 'vklad') {
			$.ajax({
				url: '/getformvklad',
				cache: false,
				success: function(html){
					$('form#validate').replaceWith(html);
					if (!Modernizr.input.placeholder){
						$('form#validate [placeholder]').placeholder();
					}
					if (window.PIE) {
						$('form#validate .pie').each(function() {
							PIE.attach(this);
						});
					}
					$('form#validate .style-select').ikSelect({customClass: 'style-select'});
					$('form#validate .style-select2').ikSelect({autoWidth: false, customClass: 'style-select2'});
				}
			});
		}

		if($(this).val() == 'consumercredit') {
			$.ajax({
				url: '/getformconsumercredit',
				cache: false,
				success: function(html){
					$('form#validate').replaceWith(html);
					if (!Modernizr.input.placeholder){
						$('form#validate [placeholder]').placeholder();
					}
					if (window.PIE) {
						$('form#validate .pie').each(function() {
							PIE.attach(this);
						});
					}
					$('form#validate .style-select').ikSelect({customClass: 'style-select'});
					$('form#validate .style-select2').ikSelect({autoWidth: false, customClass: 'style-select2'});
				}
			});
		}

		if($(this).val() == 'autocredit') {
			$.ajax({
				url: '/getformautocredit',
				cache: false,
				success: function(html){
					$('form#validate').replaceWith(html);
					if (!Modernizr.input.placeholder){
						$('form#validate [placeholder]').placeholder();
					}
					if (window.PIE) {
						$('form#validate .pie').each(function() {
							PIE.attach(this);
						});
					}
					$('form#validate .style-select').ikSelect({customClass: 'style-select'});
					$('form#validate .style-select2').ikSelect({autoWidth: false, customClass: 'style-select2'});
				}
			});
		}

		if($(this).val() == 'creditcard') {
			$.ajax({
				url: '/getformcreditcard',
				cache: false,
				success: function(html){
					$('form#validate').replaceWith(html);
					if (!Modernizr.input.placeholder){
						$('form#validate [placeholder]').placeholder();
					}
					if (window.PIE) {
						$('form#validate .pie').each(function() {
							PIE.attach(this);
						});
					}
					$('form#validate .style-select').ikSelect({customClass: 'style-select'});
					$('form#validate .style-select2').ikSelect({autoWidth: false, customClass: 'style-select2'});
				}
			});
		}

	});

	$('body').on('click', '.clcurl', function() {
		window.location = $(this).attr('value');
	});

	$('body').on('submit', 'form#validate', function() {
		var rv_name = /^[0-9\.]*$/;

		var valmin = $(this).find('input[name="minmonth"]').val();
		var valmax = $(this).find('input[name="maxmonth"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minmonth"]').addClass('has-error');
			else
				$(this).find('input[name="minmonth"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minmonth"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxmonth"]').addClass('has-error');
			else
				$(this).find('input[name="maxmonth"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxmonth"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxmonth"]').addClass('has-error');
				$(this).find('input[name="minmonth"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minmonth"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxmonth"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minlimit"]').val();
		valmax = $(this).find('input[name="maxlimit"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minlimit"]').addClass('has-error');
			else
				$(this).find('input[name="minlimit"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minlimit"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxlimit"]').addClass('has-error');
			else
				$(this).find('input[name="maxlimit"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxlimit"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxlimit"]').addClass('has-error');
				$(this).find('input[name="minlimit"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minlimit"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxlimit"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minprice"]').val();
		valmax = $(this).find('input[name="maxprice"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minprice"]').addClass('has-error');
			else
				$(this).find('input[name="minprice"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minprice"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxprice"]').addClass('has-error');
			else
				$(this).find('input[name="maxprice"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxprice"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxprice"]').addClass('has-error');
				$(this).find('input[name="minprice"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minprice"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxprice"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minday"]').val();
		valmax = $(this).find('input[name="maxday"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minday"]').addClass('has-error');
			else
				$(this).find('input[name="minday"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minday"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxday"]').addClass('has-error');
			else
				$(this).find('input[name="maxday"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxday"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxday"]').addClass('has-error');
				$(this).find('input[name="minday"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minday"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxday"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minrate"]').val();
		valmax = $(this).find('input[name="maxrate"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minrate"]').addClass('has-error');
			else
				$(this).find('input[name="minrate"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minrate"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxrate"]').addClass('has-error');
			else
				$(this).find('input[name="maxrate"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxrate"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxrate"]').addClass('has-error');
				$(this).find('input[name="minrate"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minrate"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxrate"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minvznos"]').val();
		valmax = $(this).find('input[name="maxvznos"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minvznos"]').addClass('has-error');
			else
				$(this).find('input[name="minvznos"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minvznos"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxvznos"]').addClass('has-error');
			else
				$(this).find('input[name="maxvznos"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxvznos"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxvznos"]').addClass('has-error');
				$(this).find('input[name="minvznos"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minvznos"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxvznos"]:not(.has-error)').removeClass('has-error');
			}
		}

		if($(this).find('.has-error').length){
		    if(!$(this).find('.has-error-txt').length)
			$('#validate button').after('<p class="has-error-txt">Неверно введены параметры поиска</p>');
		    return false;
		}
	});


	$('form#validate-hypothec').submit(function() {
		var rv_name = /^[0-9\.]*$/;

		var valmin = $(this).find('input[name="minvznos"]').val();
		var valmax = $(this).find('input[name="maxvznos"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minvznos"]').addClass('has-error');
			else
				$(this).find('input[name="minvznos"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minvznos"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxvznos"]').addClass('has-error');
			else
				$(this).find('input[name="maxvznos"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxvznos"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxvznos"]').addClass('has-error');
				$(this).find('input[name="minvznos"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minvznos"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxvznos"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minday"]').val();
		valmax = $(this).find('input[name="maxday"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minday"]').addClass('has-error');
			else
				$(this).find('input[name="minday"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minday"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxday"]').addClass('has-error');
			else
				$(this).find('input[name="maxday"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxday"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxday"]').addClass('has-error');
				$(this).find('input[name="minday"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minday"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxday"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minprice"]').val();
		valmax = $(this).find('input[name="maxprice"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minprice"]').addClass('has-error');
			else
				$(this).find('input[name="minprice"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minprice"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxprice"]').addClass('has-error');
			else
				$(this).find('input[name="maxprice"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxprice"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxprice"]').addClass('has-error');
				$(this).find('input[name="minprice"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minprice"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxprice"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minrate"]').val();
		valmax = $(this).find('input[name="maxrate"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minrate"]').addClass('has-error');
			else
				$(this).find('input[name="minrate"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minrate"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxrate"]').addClass('has-error');
			else
				$(this).find('input[name="maxrate"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxrate"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxrate"]').addClass('has-error');
				$(this).find('input[name="minrate"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minrate"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxrate"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minage"]').val();
		valmax = $(this).find('input[name="maxage"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minage"]').addClass('has-error');
			else
				$(this).find('input[name="minage"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minage"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxage"]').addClass('has-error');
			else
				$(this).find('input[name="maxage"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxage"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxage"]').addClass('has-error');
				$(this).find('input[name="minage"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minage"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxage"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minageman"]').val();
		valmax = $(this).find('input[name="maxageman"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minageman"]').addClass('has-error');
			else
				$(this).find('input[name="minageman"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minageman"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxageman"]').addClass('has-error');
			else
				$(this).find('input[name="maxageman"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxageman"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxageman"]').addClass('has-error');
				$(this).find('input[name="minageman"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minageman"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxageman"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minagewomen"]').val();
		valmax = $(this).find('input[name="maxagewomen"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minagewomen"]').addClass('has-error');
			else
				$(this).find('input[name="minagewomen"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minagewomen"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxagewomen"]').addClass('has-error');
			else
				$(this).find('input[name="maxagewomen"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxagewomen"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxagewomen"]').addClass('has-error');
				$(this).find('input[name="minagewomen"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minagewomen"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxagewomen"]:not(.has-error)').removeClass('has-error');
			}
		}

		if($(this).find('.has-error').length){
		    if(!$(this).find('.has-error-txt').length) {
				$('#validate button').after('<p class="has-error-txt">Неверно введены параметры поиска</p>');
				$('#validate-hypothec button').after('<p class="has-error-txt">Неверно введены параметры поиска</p>');
			}
		    return false;
		}

	});

	$('form#validate-consumercredit').submit(function() {
		var rv_name = /^[0-9\.]*$/;

		valmin = $(this).find('input[name="minday"]').val();
		valmax = $(this).find('input[name="maxday"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minday"]').addClass('has-error');
			else
				$(this).find('input[name="minday"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minday"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxday"]').addClass('has-error');
			else
				$(this).find('input[name="maxday"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxday"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxday"]').addClass('has-error');
				$(this).find('input[name="minday"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minday"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxday"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minprice"]').val();
		valmax = $(this).find('input[name="maxprice"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minprice"]').addClass('has-error');
			else
				$(this).find('input[name="minprice"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minprice"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxprice"]').addClass('has-error');
			else
				$(this).find('input[name="maxprice"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxprice"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxprice"]').addClass('has-error');
				$(this).find('input[name="minprice"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minprice"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxprice"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minrate"]').val();
		valmax = $(this).find('input[name="maxrate"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minrate"]').addClass('has-error');
			else
				$(this).find('input[name="minrate"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minrate"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxrate"]').addClass('has-error');
			else
				$(this).find('input[name="maxrate"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxrate"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxrate"]').addClass('has-error');
				$(this).find('input[name="minrate"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minrate"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxrate"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minage"]').val();
		valmax = $(this).find('input[name="maxage"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minage"]').addClass('has-error');
			else
				$(this).find('input[name="minage"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minage"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxage"]').addClass('has-error');
			else
				$(this).find('input[name="maxage"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxage"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxage"]').addClass('has-error');
				$(this).find('input[name="minage"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minage"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxage"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minageman"]').val();
		valmax = $(this).find('input[name="maxageman"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minageman"]').addClass('has-error');
			else
				$(this).find('input[name="minageman"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minageman"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxageman"]').addClass('has-error');
			else
				$(this).find('input[name="maxageman"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxageman"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxageman"]').addClass('has-error');
				$(this).find('input[name="minageman"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minageman"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxageman"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minagewomen"]').val();
		valmax = $(this).find('input[name="maxagewomen"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minagewomen"]').addClass('has-error');
			else
				$(this).find('input[name="minagewomen"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minagewomen"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxagewomen"]').addClass('has-error');
			else
				$(this).find('input[name="maxagewomen"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxagewomen"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxagewomen"]').addClass('has-error');
				$(this).find('input[name="minagewomen"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minagewomen"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxagewomen"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minsrok"]').val();
		valmax = $(this).find('input[name="maxsrok"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minsrok"]').addClass('has-error');
			else
				$(this).find('input[name="minsrok"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minsrok"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxsrok"]').addClass('has-error');
			else
				$(this).find('input[name="maxsrok"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxsrok"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxsrok"]').addClass('has-error');
				$(this).find('input[name="minsrok"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minsrok"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxsrok"]:not(.has-error)').removeClass('has-error');
			}
		}

		valmin = $(this).find('input[name="minstage"]').val();
		valmax = $(this).find('input[name="maxstage"]').val();
        if(valmin != '') {
			if(!rv_name.test(valmin))
				$(this).find('input[name="minstage"]').addClass('has-error');
			else
				$(this).find('input[name="minstage"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="minstage"]').removeClass('has-error');
		if(valmax != '') {
			if(!rv_name.test(valmax))
				$(this).find('input[name="maxstage"]').addClass('has-error');
			else
				$(this).find('input[name="maxstage"]').removeClass('has-error');
        }
		else
			$(this).find('input[name="maxstage"]').removeClass('has-error');
		if(valmax != '' && valmin != '') {
			if(valmax * 1 < valmin * 1) {
				$(this).find('input[name="maxstage"]').addClass('has-error');
				$(this).find('input[name="minstage"]').addClass('has-error');
			}
			else {
				$(this).find('input[name="minstage"]:not(.has-error)').removeClass('has-error');
				$(this).find('input[name="maxstage"]:not(.has-error)').removeClass('has-error');
			}
		}

		if($(this).find('.has-error').length){
		    if(!$(this).find('.has-error-txt').length) {
				$('#validate button').after('<p class="has-error-txt">Неверно введены параметры поиска</p>');
				$('#validate-consumercredit button').after('<p class="has-error-txt">Неверно введены параметры поиска</p>');
			}
		    return false;
		}

	});

	var idkuhpcalc = null;
	$('form input[type="text"]').keyup(function(){
		var cont = this;
		window.clearTimeout(idkuhpcalc);
		idkuhpcalc = window.setTimeout(function(){
			var rv_name = /^[0-9\.]+$/;
			var valprice = $(cont).closest('form').find('input[name="price"]').val();
			var valvznos = $(cont).closest('form').find('input[name="vznos"]').val();
			var tmp = '';
			if(rv_name.test(valprice) && rv_name.test(valvznos)) {
				tmp = valprice * valvznos / 100;
			}
			$(cont).closest('form').find('input[name="vznosdengi"]').val(tmp);
		}, 333);
	});

	$('form#validate-calchypothec').submit(function() {
		var rv_name = /^[0-9\.]+$/;
		var rv_name_c = /^[0-9]+$/;

		$(this).find('.invalid').removeClass('invalid');
		$('#result-calc-hypothec').html("");

		//
		var valsrok = $(this).find('input[name="srok"]').val();
		var srokmax = $(this).find('input[name="srok"]').data('max');
		var srokmin = $(this).find('input[name="srok"]').data('min');
		var period = $(this).find('input[name="period"]:checked').attr('value');

		if(!rv_name_c.test(valsrok)) {
			$(this).find('input[name="srok"]').addClass('invalid');
		}
		else {
			if(rv_name.test(srokmax) && (srokmax * 1) > 0) {
				if(period == 'month') srokmax = srokmax * 12;
				if((valsrok * 1) > (srokmax * 1)) $(this).find('input[name="srok"]').addClass('invalid');
			}
			if(rv_name.test(srokmin) && (srokmin * 1) > 0) {
				if(period == 'month') srokmin = srokmin * 12;
				if((valsrok * 1) < (srokmin * 1)) $(this).find('input[name="srok"]').addClass('invalid');
			}
		}

		//
		var valvznos = $(this).find('input[name="vznos"]').val();
		var vznosmax = $(this).find('input[name="vznos"]').data('max');
		var vznosmin = $(this).find('input[name="vznos"]').data('min');

		if(!rv_name.test(valvznos)) {
			$(this).find('input[name="vznos"]').addClass('invalid');
		}
		else {
			if(rv_name.test(vznosmax) && (vznosmax * 1) > 0) {
				if((valvznos * 1) > (vznosmax * 1)) $(this).find('input[name="vznos"]').addClass('invalid');
			}
			if(rv_name.test(vznosmin) && (vznosmin * 1) > 0) {
				if((valvznos * 1) < (vznosmin * 1)) $(this).find('input[name="vznos"]').addClass('invalid');
			}
		}

		//
		var valprice = $(this).find('input[name="price"]').val();
		var pricemax = $(this).find('input[name="price"]').data('max');

		if(!rv_name_c.test(valprice)) {
			$(this).find('input[name="price"]').addClass('invalid');
		}
//		else {
//			if(rv_name.test(pricemax) && (pricemax * 1) > 0) {
//				if((valprice * 1) > (pricemax * 1)) $(this).find('input[name="price"]').addClass('invalid');
//			}
//		}

		//
		if($(this).find('.invalid').length) {
			$('#result-calc-hypothec').html("Введены недопустимые параметры продукта");
			return false;
		}
		else {
			var tmp = valprice * valvznos / 100;
			valprice = valprice - tmp;
			if(rv_name.test(pricemax) && (pricemax * 1) > 0) {
				if((valprice * 1) > (pricemax * 1)) $(this).find('input[name="price"]').addClass('invalid');
			}
			if($(this).find('.invalid').length) {
				$('#result-calc-hypothec').html("Недопустимая сумма кредита – " + valprice + "<br/> Максимальный размер кредита составляет " + (pricemax * 1) + " ");
				return false;
			}
		}

	});

	$('form#validate-calccredit').submit(function() {
		var rv_name = /^[0-9\.]+$/;
		var rv_name_c = /^[0-9]+$/;

		$(this).find('.invalid').removeClass('invalid');
		$('#result-calc-hypothec').html("");

		//
		var valsrok = $(this).find('input[name="srok"]').val();
		var srokmax = $(this).find('input[name="srok"]').data('max');
		var srokmin = $(this).find('input[name="srok"]').data('min');
		var period = $(this).find('input[name="period"]:checked').attr('value');

		if(!rv_name_c.test(valsrok)) {
			$(this).find('input[name="srok"]').addClass('invalid');
		}
		else {
			if(rv_name.test(srokmax) && (srokmax * 1) > 0) {
				if(period == 'month') srokmax = srokmax / 30;
				if((valsrok * 1) > (srokmax * 1)) $(this).find('input[name="srok"]').addClass('invalid');
			}
			if(rv_name.test(srokmin) && (srokmin * 1) > 0) {
				if(period == 'month') srokmin = srokmin / 30;
				if((valsrok * 1) < (srokmin * 1)) $(this).find('input[name="srok"]').addClass('invalid');
			}
		}

		//
		var valprice = $(this).find('input[name="price"]').val();
		var pricemax = $(this).find('input[name="price"]').data('max');
		var pricemin = $(this).find('input[name="price"]').data('min');

		if(!rv_name.test(valprice)) {
			$(this).find('input[name="price"]').addClass('invalid');
		}
		else {
			if(rv_name.test(pricemax) && (pricemax * 1) > 0) {
				if((valprice * 1) > (pricemax * 1)) $(this).find('input[name="price"]').addClass('invalid');
			}
			if(rv_name.test(pricemin) && (pricemin * 1) > 0) {
				if((valprice * 1) < (pricemin * 1)) $(this).find('input[name="price"]').addClass('invalid');
			}
		}

		//
		if($(this).find('.invalid').length) {
			$('#result-calc-hypothec').html("Введены недопустимые параметры продукта");
			return false;
		}

	});

	$('body').on('click', '.basedeposit > .basedeposit-item > .basedeposit-item-title', function() {
		var $t = $(this)
			,$parent = $t.parent()
			,$spoiler = $t.next()
			;

		if($parent.hasClass('active')) {
			$parent.removeClass('active');
			$spoiler.slideUp(400);
			return;
		}
		$('.container .basedeposit > .basedeposit-item')
		.removeClass('active')
		.find('.basedeposit-item-sub')
		.slideUp(400)
		.find('.b-write-item-wrap')
		.removeClass('b-active')
		;
		$parent.addClass('active');
		$spoiler.slideDown(400);
	});

	$(".scrbx").mCustomScrollbar({
		axis: "x",
		theme:"dol-theme"
	});

	$('body').on('click', '.other1, .other2', function(){
		$(this).closest('.drpdwn-m').toggleClass('act');
	});
	/*
	 * IkSelect tooltip
	 */
	/*$("body").append("<div id='tooltipstr'></div>");
	$('#tooltipstr').css({position: 'absolute', top: '-9999px', left: '-9999px', border: '1px solid #ccc', padding: '5px', 'z-index': '9999', background: '#fff'}).hide()
	$('body').on('mouseover', '.ik_select_option', function(){
		var p = $(this).offset();
		$('#tooltipstr').text($(this).text()).css({left: p.left + $(this).outerWidth() + 5, top: p.top - 10 }).show();
	});
	$('body').on('mouseout', '.ik_select_option', function(e){
		$('#tooltipstr').hide();
	});*/

	$(document).on('click', '.fav', function (e) {
		var $t = $(this);
		$t.parent().toggleClass('favorite');
		if ($t.parent().hasClass('favorite')) {
			$.ajax({
				url: '/managefavorite/set',
				data: {
					id: $(this).data('id'),
					type: $(this).data('type')
				},
				method: 'post',
				dataType: 'html',
				cache: false,
				success: function(html){

				}
			});
			$t.text('Убрать из избранного');
		} else {
			$.ajax({
				url: '/managefavorite/delete',
				data: {
					id: $(this).data('id'),
					type: $(this).data('type')
				},
				method: 'post',
				dataType: 'html',
				cache: false,
				success: function(html){

				}
			});
			$t.text('Добавить в избранное');
		}
	});

	$(document).on('click', '.fav2', function (e) {
		var $t = $(this);
		$t.parent().toggleClass('fav2orite');
		if ($t.parent().hasClass('fav2orite')) {
			$.ajax({
				url: '/managefavorite/set',
				data: {
					id: $(this).data('id'),
					type: $(this).data('type')
				},
				method: 'post',
				dataType: 'html',
				cache: false,
				success: function(html){

				}
			});
			$t.html('Убрать<br/> из избранного');
		} else {
			$.ajax({
				url: '/managefavorite/delete',
				data: {
					id: $(this).data('id'),
					type: $(this).data('type')
				},
				method: 'post',
				dataType: 'html',
				cache: false,
				success: function(html){

				}
			});
			$t.html('Добавить<br/> в избранное');
		}
	});

	$(document).on('mouseenter', '.fav', function (e) {
		$(this).parent().addClass('hover-fav');
	});

	$(document).on('mouseleave', '.fav', function (e) {
		$(this).parent().removeClass('hover-fav');
	});

	$(document).on('click', '.b-write-item-more', function() {
		$(this).closest('.b-write-item-wrap').toggleClass('b-active');
		return false;
	});

	$(document).on('click', '.bnclc', function() {
		var u = $(this).parent().data('u');
		if(u) $.get(u);
	});

	$(document).on('click', '.item-map .more-items', function() {
		$(this).closest('.item-map').toggleClass('active');
		return false;
	});
});